#ifndef TERNAIRE_H
#define TERNAIRE_H

#include "expression.h"

/**
 * \brief Représente les opérateurs ternaires.
 * Cette classe comporte des méthodes virtuelles.
 */
class Ternaire : public Expression
{
public:
    virtual const string toString() const;
    virtual void dotWhat(stringstream &vertices, stringstream &edges) const;
    
protected:
    Ternaire(Expression *, Expression *, Expression *);

    Expression *e1, *e2, *e3;

    virtual ostream& what(ostream &) const = 0;
    
private:
    Ternaire(const Ternaire&);
    Ternaire &operator=(const Ternaire&);
};

#endif // TERNAIRE_H

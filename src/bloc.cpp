//#include <iostream>
#include <cstring>

#include "bloc.h"

Bloc::Bloc(const char * name, Expression* inst) : _pool(), _name(name)
{
    if (inst)
        add(inst);
}

double Bloc::eval()
{
    double tmp(0);
    for (Expression * e : _pool) {
        tmp = e->eval();
    }
    return tmp;
}

Expression *Bloc::clone()
{
    return new Bloc(*this);
}

Expression *Bloc::simplify()
{
    return this;
}

void Bloc::add(Expression * e)
{
    _pool.push_back(e);
}

ostream &Bloc::what(ostream & os) const
{
    os << "{" << endl;
    for(Expression * e : _pool)
        os << "\t" << *e << endl;
    return os << "}";
}

Bloc::Bloc(const Bloc& b) : _pool(), _name(0) {

    for(Expression *e : b._pool) {
        _pool.push_back(e->clone());
    }
    memcpy((void*)_name, (void*)b._name, sizeof(char)*strlen(b._name));
}

const string Bloc::toString() const {
    return "{ ... }";
}

void Bloc::dotWhat(stringstream &vertices, stringstream &edges) const {
	vertices << (size_t)this;
	vertices << " [label=\"" << toString() << "\"];" << endl;
	
	for (Expression *e : _pool) {
		edges << (size_t)this;
		edges << " -> " << (size_t)e;
		edges << ";" << endl;
	}
	
	for (Expression *e : _pool) {
		e->dotWhat(vertices, edges);
	}
}

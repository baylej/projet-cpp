#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <set>
#include <ostream>
#include <sstream>
#include <string>

#include <stdexcept>

using namespace std;

/**
 * \brief Expression est la classe de base dont héritent tous les noeuds de l'arbre d'expression.
 * Cette classe comporte des méthodes virtuelles.
 */
class Expression
{
public:
	/// calcule la valeur à ce noeud dans l'arbre d'expression.
    virtual double eval() = 0;

    /// Retourne une copie défensive de ce noeud.
    virtual Expression* clone() = 0;

    /// Calcule la dérivée de l'expression depuis de noeud.
    virtual Expression* derive(const string&);

    /// Simplifie l'expression depuis ce noeud.
    virtual Expression* simplify() = 0;

    /// Cette méthode statique libère la totalité des noeuds crées.
    /// \sa _pool
    static void toutLiberer();

    friend ostream & operator<< (ostream &, const Expression &);
    
    /// Renvoit une très courte string décrivant ce noeud.
    virtual const string toString() const = 0;
    
    /// Renvoit un script DOT pour rendre une image de l'arbre d'expression avec DOT de GraphViz.
    string makeDot() const;
    /// Permet de construire un arbre d'expression visualisable avec l'outil DOT de GraphViz.
    virtual void dotWhat(stringstream &vertices, stringstream &edges) const;

    virtual ~Expression();

protected:
    Expression();

    /// Renvoit une description de ce noeud, utilisé par l'opérateur '<<'.
    virtual ostream& what(ostream &) const = 0;

private:
	/// Dans ce Set sont sauvegardées toutes les références de expressions crées.
	/// \sa toutLiberer()
    static set<Expression*> _pool;
};

#endif // EXPRESSION_H

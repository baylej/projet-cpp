#include <cmath>

#include "exp.h"

Exp::Exp(Expression * e1) : Unaire::Unaire(e1)
{
}

double Exp::eval()
{
    return exp(e1->eval());
}

Expression *Exp::clone()
{
    return new Exp(e1->clone());
}

Expression *Exp::_derive(const string &)
{
    return new Exp(e1);
}

const string Exp::toString() const {
    return "exp";
}

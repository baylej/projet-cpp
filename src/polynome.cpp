#include "polynome.h"

#include <iostream>
#include <cmath>

Polynome::Polynome(Variable &_v) : Expression::Expression(), v(_v), coeffs(10), highestDegree(0)
{
}

Polynome::Polynome(const Polynome & p) : v(p.v), coeffs(p.coeffs), highestDegree(p.highestDegree)
{
}

void Polynome::set(int coeff, unsigned int degree)
{
    //resize it if necessary
    if(degree > coeffs.size()) {
        unsigned int new_size = coeffs.size();
        while(new_size < degree)
            new_size *= 2;

        coeffs.resize(new_size);
    }
    coeffs[degree] = coeff;
    highestDegree = max(degree, highestDegree);
}

double Polynome::eval()
{
    vector<int> fac2 = vector<int>(highestDegree + 1);
    fac2[highestDegree] = coeffs[highestDegree];
    int x = v.eval();

    for(int i = (int)highestDegree - 1; i >= 0; i--) {
        fac2[i] = fac2[i+1]*x + coeffs[i];
    }
    return (double)(fac2[0]);
}

Expression *Polynome::derive(const string & v)
{
    Polynome *p = new Polynome(*this);

    if(*(new Variable(v)) == this->v) {
        for (unsigned int i = 0; i<p->coeffs.size()-1; i++) {
            p->coeffs[i] = p->coeffs[i+1] * (i+1);
        }
        p->coeffs[p->coeffs.size()-1] = 0;
    }
    return p;
}

Expression *Polynome::simplify()
{
    return this;
}

Expression *Polynome::clone()
{
    return new Polynome(*this);
}

Polynome Polynome::operator +(Polynome &p) const
{
    return *binaryOperator(p,*(new Polynome(p)),[](int a, int b) {return a+b;});
}

Polynome Polynome::operator -(Polynome &p) const
{
    return *binaryOperator(p,*(new Polynome(p)),[](int a, int b) {return a-b;});
}

Polynome Polynome::operator *(Polynome &p) const
{
    return *binaryOperator(p,*(new Polynome(p)), [](int a, int b) {return a*b;});
}

Polynome& Polynome::operator =(Polynome & p)
{
    coeffs = p.coeffs;
    v = p.v;
    highestDegree = p.highestDegree;
    return *this;
}

Polynome &Polynome::operator +=(Polynome & p)
{
    return *binaryOperator(p, *this, [](int a, int b) {return a+b;});
}

Polynome &Polynome::operator -=(Polynome & p)
{
    return *binaryOperator(p, *this, [](int a, int b) {return a-b;});
}

Polynome &Polynome::operator *=(Polynome & p)
{
    return *binaryOperator(p, *this, [](int a, int b) {return a*b;});
}

bool Polynome::operator ==(Polynome & p) const
{
    const vector<int> *v = (coeffs.size() <= p.coeffs.size()) ?
        &p.coeffs : &coeffs;

    for(unsigned int i = 0 ; i< v->size(); i++) {
        if( p.coeffs[i] != coeffs[i] )
            return false;
    }
    return true;
}

bool Polynome::operator !=(Polynome & p) const
{
    return !(*this == p);
}

ostream &Polynome::what(ostream & os) const
{
    bool show(false);

    for(unsigned int i = 0; i<coeffs.size(); i++) {
        if(coeffs[i] != 0) {
            int c = coeffs[i];
            if(i == 0) {
                os << c << " ";
                show = true;
            }
            else {
                string sign = (c >=0)?"+":"-";
                if(abs(c) == 1) {
                    os << sign << " " << v << "^" << i << " ";
                    show = true;
                }
                else if(show) {
                    os << sign << " " << abs(c) << "." << v << "^" << i << " ";
                    show = true;
                } else {
                    os << c << "." << v << "^" << i << " ";
                    show = true;
                }
            }
        }
    }
    return os;
}

Polynome *Polynome::binaryOperator(Polynome &in, Polynome &out, int(*f)(int, int)) const
{
    if(v == in.v) {
        const vector<int> *v = (coeffs.size() <= in.coeffs.size()) ?
            &in.coeffs : &coeffs;

        for(unsigned int i = 0 ; i< v->size(); i++) {
            out.set(f(in.coeffs[i], coeffs[i]), i);
        }
    }
    return &out;
}

const string Polynome::toString() const {
    return "Polynome";
}
/*
void Polynome::dotWhat(stringstream &vertices, stringstream &edges) const {
    // TODO
}*/

#ifndef OR_H
#define OR_H

#include "binaire.h"

/**
 * \brief Ou logique.
 * \ingroup binaires
 */
class Or : public Binaire
{
public:
    Or(Expression *,Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // OR_H

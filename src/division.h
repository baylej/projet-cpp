#ifndef DIVISION_H
#define DIVISION_H

#include "binaire.h"

/**
 * \brief Opérateur division.
 * \ingroup binaires
 */
class Division : public Binaire
{
public:
    Division(Expression *,Expression *);

    virtual double eval();

    virtual Expression* derive(const string&);

    virtual Expression* simplify();

    virtual Expression* clone();
    
    virtual const string toString() const;
};

#endif // DIVISION_H

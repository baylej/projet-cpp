#ifndef NONEGAL_H
#define NONEGAL_H

#include "binaire.h"

/**
 * \brief Comparateur d'inégalité "!=".
 * \ingroup binaires
 * \ingroup comparateurs
 */
class NonEgal : public Binaire
{
public:
    NonEgal(Expression *,Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // NONEGAL_H

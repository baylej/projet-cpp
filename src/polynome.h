#ifndef POLYNOME_H
#define POLYNOME_H

#include <vector>
#include <functional>

#include "expression.h"
#include "variable.h"

class Polynome : public Expression
{
public:
	/// Crée un polynome avec son inconnue.
    Polynome(Variable &);

    Polynome(const Polynome&);

    /// Assigne un coefficient au terme du dégré donné.
    void set(int coeff, unsigned int degree);

    virtual double eval();

    virtual Expression* derive(const string&);

    virtual Expression* simplify();

    virtual Expression* clone();
    
    virtual const string toString() const;
    //virtual void dotWhat(stringstream &vertices, stringstream &edges) const;

    //Operator overload +, -, *, =, +=, -=, *=, ==, !=.

    Polynome operator +(Polynome&) const;
    Polynome operator -(Polynome&) const;
    Polynome operator *(Polynome&) const;

    Polynome& operator =(Polynome&);

    Polynome& operator +=(Polynome&);
    Polynome& operator -=(Polynome&);
    Polynome& operator *=(Polynome&);

    bool operator ==(Polynome&) const;
    bool operator !=(Polynome&) const;

protected:
    virtual ostream& what(ostream &) const;
    
private:
	/// Nom de l'inconnue.
    Variable v;
    /// Tableau des coefficients.
    vector<int> coeffs;
    /// Degré le plus grand.
    unsigned int highestDegree;

    Polynome * binaryOperator(Polynome &in, Polynome &out, int(*f)(int, int)) const;

    Polynome(Variable _v, vector<int> _coeffs, int _highestCoeff);
};

#endif // POLYNOME_H

#include <sstream>

#include "somme.h"
#include "variable.h"
#include "produit.h"
#include "constante.h"

Somme::Somme(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double Somme::eval()
{
    return e1->eval() + e2->eval();
}

Expression *Somme::derive(const string & v)
{
    return new Somme(e1->derive(v), e2->derive(v));
}

Expression *Somme::simplify()
{
    Variable * v1 = dynamic_cast<Variable*> (e1);
    Variable * v2 = dynamic_cast<Variable*> (e2);

    Constante * c1 = dynamic_cast<Constante*> (e1);
    Constante * c2 = dynamic_cast<Constante*> (e2);

    if (v1 && v2) {
        if(*v1 == *v2) {
            stringstream ss; ss << *v1;
            return new Produit(new Constante(2), new Variable(ss.str()));
        }
    } else if(c1 && c2) {
        return new Constante(c1->eval() + c2->eval());
    }
    Expression *s1 = e1->simplify();
    Expression *s2 = e2->simplify();

    if(s1 != e1 && s2 != e2) {
        return (new Somme(e1->simplify(), e2->simplify()))->simplify();
    }
    else if(s1 != e1) {
        return (new Somme(e1->simplify(), e2))->simplify();
    }
    else if(s2 != e2) {
        return (new Somme(e1, e2->simplify()))->simplify();
    }
    return this;
}

Expression *Somme::clone()
{
    return new Somme(e1->clone(), e2->clone());
}

const string Somme::toString() const {
    return "+";
}

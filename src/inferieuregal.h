#ifndef INFERIEUREGAL_H
#define INFERIEUREGAL_H

#include "binaire.h"

/**
 * \brief Comparateur d'infériorité "<=".
 * \ingroup binaires
 * \ingroup comparateurs
 */
class InferieurEgal : public Binaire
{
public:
    InferieurEgal(Expression *,Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // INFERIEUREGAL_H

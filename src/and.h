#ifndef AND_H
#define AND_H

#include "binaire.h"

/**
 * \brief Et logique.
 * \ingroup binaires
 */
class And : public Binaire
{
public:
    And(Expression *,Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // AND_H

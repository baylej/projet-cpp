#include "unaire.h"
#include "produit.h"

#include "constante.h"

Expression *Unaire::derive(const string & v)
{
    return new Produit(e1->derive(v), _derive(v));
}

Unaire::Unaire(Expression * _e1) : Expression::Expression(), e1(_e1)
{
    if(!_e1)
        throw std::invalid_argument("Unaire operator should always have !=NUll argument");
}

Expression *Unaire::_derive(const string &)
{
    return new Constante(0);
}

Expression *Unaire::simplify()
{
    e1 = e1->simplify();
    return this;
}

ostream &Unaire::what(ostream & os) const
{
    return os << toString() << "(" << *e1 << ")";
}

void Unaire::dotWhat(stringstream &vertices, stringstream &edges) const {
	vertices << (size_t)this;
	vertices << " [label=\"" << toString() << "\"];" << endl;
	
	edges << (size_t)this;
	edges << " -> " << (size_t)e1;
	edges << ";" << endl;
	
	e1->dotWhat(vertices, edges);
}

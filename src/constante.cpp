#include <sstream>
#include <cstdio>

#include "constante.h"

Constante::Constante(double _value) : Expression::Expression(), value(_value)
{
}

double Constante::eval()
{
    return value;
}

Expression *Constante::derive(const string &)
{
    return new Constante(0);
}

Expression *Constante::clone()
{
    return new Constante(value);
}

Expression *Constante::simplify()
{
    return this;
}

ostream& Constante::what(ostream & os) const
{
    if(value < 0)
        return os << "(" << value << ")";
    return os << value;
}

const string Constante::toString() const {
	char val[32];
	snprintf(val, 32, "%lf", value);
	return val;
}


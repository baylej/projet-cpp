#ifndef BINAIRE_H
#define BINAIRE_H

#include "expression.h"

/**
 * \brief Représente les opérateurs binaires.
 * Cette classe comporte des méthodes virtuelles.
 */
class Binaire : public Expression
{
public:
    virtual void dotWhat(stringstream &vertices, stringstream &edges) const;
protected:
    Binaire(Expression *, Expression *);

    Expression *e1, *e2;

    virtual ostream& what(ostream &) const;

private:
    Binaire(const Binaire&);
    Binaire &operator=(const Binaire&);
};

#endif // BINAIRE_H

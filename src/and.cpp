#include "and.h"

And::And(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double And::eval()
{
    return e1->eval() && e2->eval();
}

Expression *And::clone()
{
    return new And(e1,e2);
}

Expression *And::simplify()
{
    return this;
}

const string And::toString() const {
	return "&&";
}

#include <iostream>

#include "produit.h"
#include "somme.h"
#include "variable.h"
#include "constante.h"


Produit::Produit(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double Produit::eval()
{
    return e1->eval() * e2->eval();
}

Expression *Produit::derive(const string & v)
{
    return new Somme(new Produit(e1->derive(v), e2), new Produit(e2->derive(v), e1));
}

Expression *Produit::simplify()
{
    Constante *c1 = dynamic_cast<Constante*>(e1);
    Constante *c2 = dynamic_cast<Constante*>(e2);

    if(c1 && c2) {
        return new Constante(c1->eval() * c2->eval());
    } else if(c2) {
       if(c2->eval() == 1.0) {
           return e1->simplify();
       }
       else if(c2->eval() == 0.0) {
           return new Constante(0.0);
       }
    } else if(c1) {
        if(c1->eval() == 1.0) {
            return e2->simplify();
        }
        else if(c1->eval() == 0.0) {
            return new Constante(0.0);
        }
     }

     Expression *s1 = e1->simplify();
     Expression *s2 = e2->simplify();

     if(s1 != e1 && s2 != e2) {
         return (new Produit(e1->simplify(), e2->simplify()))->simplify();
     }
     else if(s1 != e1) {
         return (new Produit(e1->simplify(), e2))->simplify();
     }
     else if(s2 != e2) {
         return (new Produit(e1, e2->simplify()))->simplify();
     }
     return this;
}

Expression *Produit::clone()
{
    return new Produit(e1->clone(), e2->clone());
}

const string Produit::toString() const {
    return "*";
}

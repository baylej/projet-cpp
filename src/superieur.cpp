#include "superieur.h"

Superieur::Superieur(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double Superieur::eval()
{
    return (e1->eval() > e2->eval())?1:0;
}

Expression *Superieur::simplify()
{
    return new Superieur(e1->simplify(), e2->simplify());
}

Expression *Superieur::clone()
{
    return new Superieur(e1->clone(), e2->clone());
}

const string Superieur::toString() const {
    return ">";
}

#ifndef VARIABLE_H
#define VARIABLE_H

#include "map"

#include "expression.h"

/** 
 * \brief Variable est un litérral, un identifiant dont la valeur peut changer.
 * \ingroup litteraux
 */
class Variable : public Expression
{
public:
	/// Crée une variable avec un nom.
    Variable(string );
    /// Crée une variable avec un nom et une valeur.
    Variable(string , double);
    /// Affecte cette variable d'une valeur.
    void set(double);

    /// Efface la mémoire (réinitialise toutes les variables à 0).
    /// \sa _pool
    static void effacerMemoire();

    virtual double eval();

    virtual Expression* derive(const string&);

    virtual Expression* simplify();

    virtual Expression* clone();

    bool operator==(const Variable &) const;
    
    virtual const string toString() const;

protected:
    virtual ostream& what(ostream &) const;

private:
	/// Table associatives des variables et leur valeur.
	/// \sa effacerMemoire()
    static map<string, double> _pool;
    /// Nom de la variable.
    string key;
};

#endif // VARIABLE_H

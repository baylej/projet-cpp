#ifndef DIFFERENCE_H
#define DIFFERENCE_H

#include "binaire.h"

/**
 * \brief Opérateur soustraction.
 * \ingroup binaires
 */
class Difference : public Binaire
{
public:
    Difference(Expression *,Expression *);

    virtual double eval();

    virtual Expression* derive(const string&);

    virtual Expression* simplify();

    virtual Expression* clone();
    
    virtual const string toString() const;
};

#endif // DIFFERENCE_H

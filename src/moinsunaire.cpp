#include "moinsunaire.h"

MoinsUnaire::MoinsUnaire(Expression * e1) : Unaire::Unaire(e1)
{
}

double MoinsUnaire::eval()
{
    return -e1->eval();
}

Expression *MoinsUnaire::clone()
{
    return new MoinsUnaire(e1->clone());
}

Expression *MoinsUnaire::simplify()
{
    return this;
}

const string MoinsUnaire::toString() const {
    return "-";
}

#include "expression.h"
#include "constante.h"

set<Expression*> Expression::_pool;

Expression * Expression::derive(const string&)
{
    return new Constante(0);
}

void Expression::toutLiberer()
{
    for(Expression* e : _pool)
        delete e;
    _pool.clear();
}

Expression::~Expression()
{
}

ostream & operator<< (ostream & os, const Expression & e) {
    return e.what(os);
}

Expression::Expression()
{
    _pool.insert(this);
}

string Expression::makeDot() const {
	stringstream vertices, edges;
	vertices << "digraph exp {" << endl;
	this->dotWhat(vertices, edges);
	edges << endl << "}";
	vertices << endl << edges.str();
	return vertices.str();
}

void Expression::dotWhat(stringstream &vertices, stringstream &edges) const {
	vertices << (size_t)this;
	vertices << " [label=\"" << toString() << "\"];" << endl;
}

#include <sstream>

#include "affectation.h"

Affectation::Affectation(Variable * v, Expression * e1) : Binaire::Binaire(v,e1)
{
}

double Affectation::eval()
{
    Variable * v = (Variable*)e1;
//    stringstream ss; ss << *e1;
//    new Variable(ss.str(), e2->eval());
    v->set(e2->eval());
    return v->eval();
}

Expression *Affectation::clone()
{
    return new Affectation(static_cast<Variable*>(e1->clone()), e2->clone());
}

Expression *Affectation::simplify()
{
    return new Affectation(static_cast<Variable*>(e1), e2->simplify());
}

const string Affectation::toString() const {
	return "<-";
}


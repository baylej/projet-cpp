#include <iostream>

#include "si_alors_sinon.h"

IfThenElse::IfThenElse(Expression * e1, Expression *e2, Expression *e3):
    Ternaire::Ternaire(e1,e2,e3)
{
    if(!(e1 && e2))
        throw std::invalid_argument("IfThenElse: should have e1 and e2 != NULL");
}

double IfThenElse::eval()
{
    if(e3)
        return (e1->eval())?e2->eval():e3->eval();
    return (e1->eval())?e2->eval():0;
}

Expression *IfThenElse::clone()
{
    return new IfThenElse(e1->clone(),e2->clone(),e3->clone());
}

Expression *IfThenElse::simplify()
{
    return new IfThenElse(e1->simplify(), e2->simplify(),
                          e3->simplify());
}

ostream &IfThenElse::what(ostream & os) const
{
    if (e3)
        return os << "if (" << *e1 << ") {" << endl
                    << "\t" << *e2 << endl
                  << "} else {" << endl
                    << "\t" << *e3 << endl
                 << "}";

    return os << "if (" << *e1 << ") {" << endl
                << "\t" << *e2 << endl
              << "}" ;

}

const string IfThenElse::toString() const {
    return "IfThenElse";
}

#include "negation.h"

Negation::Negation(Expression * e1) : Unaire::Unaire(e1)
{
}

double Negation::eval()
{
    return !e1->eval();
}

Expression *Negation::clone()
{
    return new Negation(e1->clone());
}

Expression *Negation::simplify()
{
    return new Negation(e1->simplify());
}

const string Negation::toString() const {
    return "!";
}

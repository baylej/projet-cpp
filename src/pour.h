#ifndef POUR_H
#define POUR_H

#include "expression.h"

/**
 * \brief Boucle avec initialisation, test, incrément et bloc d'instruction à répéter.
 */
class Pour : public Expression
{
public:
	/// Crée une boucle avec initialisation, test, incrément, instruction à répéter.
	/// L'initialisation ainsi que l'incrément sont optionnels et peuvent être mis à NULL.
    Pour(Expression *, Expression *, Expression *, Expression *);

    virtual double eval();

    virtual Expression* simplify();

    virtual Expression* clone();

    virtual ostream& what(ostream &) const;
    virtual string what() const {return "";}
    virtual const string toString() const;
    virtual void dotWhat(stringstream &vertices, stringstream &edges) const;

private:
    Expression *init, *cond, *loop, *body;

    Pour(const Pour&);
    Pour &operator=(const Pour&);
};

#endif // POUR_H

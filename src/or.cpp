#include "or.h"

Or::Or(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double Or::eval()
{
    return e1->eval() || e2->eval();
}

Expression *Or::clone()
{
    return new Or(e1->clone(),e2->clone());
}

Expression *Or::simplify()
{
    return new Or(e1->simplify(), e2->simplify());
}

const string Or::toString() const {
    return "||";
}

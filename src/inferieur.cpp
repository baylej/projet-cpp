#include "inferieur.h"


Inferieur::Inferieur(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double Inferieur::eval()
{
    return (e1->eval() < e2->eval())?1:0;
}

Expression *Inferieur::clone()
{
    return new Inferieur(e1->clone(),e2->clone());
}

Expression *Inferieur::simplify()
{
    return this;
}

const string Inferieur::toString() const {
    return "<";
}

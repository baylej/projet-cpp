#include <cstdio>
#include <iostream>
#include <cmath>
#include <map>
#include <vector>

#include "expression.h"

#include "sin.h"
#include "cos.h"
#include "constante.h"
#include "exp.h"
#include "moinsunaire.h"
#include "negation.h"

#include "somme.h"
#include "difference.h"
#include "produit.h"
#include "division.h"
#include "inferieuregal.h"
#include "superieuregal.h"
#include "superieur.h"
#include "inferieur.h"
#include "and.h"
#include "or.h"

#include "variable.h"
#include "affectation.h"

#include "ternaire.h"
#include "conditionnel.h"

#include "si_alors_sinon.h"
#include "pour.h"
#include "bloc.h"

#include "polynome.h"


int TEST();

using std::cout;
using std::endl;

void testUnaires() {
    Cos *c = new Cos(new Constante(M_PI/3.0));
    cout << *c << " = " << c->eval() << endl;

    MoinsUnaire * m = new MoinsUnaire(new Constante(42));
    cout << *m << " = " << m->eval() << endl;

    Sin * s = new Sin(m);
    cout << *s << " = " << s->eval() << endl;

    Exp * e = new Exp(new Division(new Produit(new Constante(2), new Constante(M_PI)), new Constante(4)));
    cout << *e << " = " << e->eval() << endl;

    MoinsUnaire *m1 = new MoinsUnaire(c);
    Somme * so = new Somme(c,m1);
    cout << *so << " = " << so->eval() << endl;

    Expression::toutLiberer();
}

void testBinaires() {
    Somme *s = new Somme( new Constante(1.0),
                          new Produit(new Constante(2.0),
                                      new Sin(new Constante(M_PI/6.0))
                                      ));
    cout << *s << " = " << s->eval() << endl;
    //SuperieurEgal comp(s, new Constante(1.8));
    //IMPOSSIBLE DE LIBRER UNE VARIABLE LOCALE
    //cout << comp << " = " << (bool)comp.eval() << endl;

    Produit * p = new Produit(
                new Somme(new Sin(new Constante(6)), new Constante(4)),
                new Somme(new Exp(new Constante(8)), new Constante(M_PI))
                          );
    cout << *p << " = " << p->eval() << endl;

    Expression::toutLiberer();
}

void testVariables() {
    //Variable x("x", 3.0);
    //Variable y("y");

    Variable *x = new Variable("x", 3.0);
    Variable *y = new Variable("y");

    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;
    Expression *exp = new Somme( new Constante(1.0),
                                 new Produit(new Constante(2.0), new Variable("x")));

    Affectation *a = new Affectation(new Variable("y"), exp);
    cout << *a << " = " << a->eval() << endl;
    cout << *y << " = " << y->eval() << endl;
    Variable::effacerMemoire();
    cout << *y << " = " << y->eval() << endl;

    Expression::toutLiberer();
}

void testConditionnel () {
    Conditionnel *test =
            new Conditionnel(new InferieurEgal(new Variable("x"), new Constante(0.0)),
                             new Cos(new Variable("x")),
                             new Cos(new Produit(new Constante(2.0), new Variable("x"))));
    Variable *x = new Variable("x", M_PI/3.0);
    cout << *x << " = " << x->eval() << endl;
    cout << *test << " = " << test->eval() << endl;
    x->set( -M_PI/3.0);
    cout << *x << " = " << x->eval() << endl;
    cout << *test << " = " << test->eval() << endl;

    Variable::effacerMemoire();
    Expression::toutLiberer();
}

void testSi_alors_sinon () {
    IfThenElse *test =
            new IfThenElse(new InferieurEgal(new Variable("x"), new Constante(0.0)),
                             new Affectation (new Variable("y"), new Cos(new Variable("x"))),
                             new Affectation ( new Variable("y"), new Cos(new Produit(new Constante(2.0), new Variable("x"))))
                           );
    Variable *x = new Variable("x", 5);
    Variable *y = new Variable("y");

    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;
    cout << *test << endl; test->eval();

    x->set( -5);
    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;

    cout << *test << endl; test->eval();
    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;

    Variable::effacerMemoire();
    Expression::toutLiberer();
}

void testSi_alors_sinon_2() {

    IfThenElse *test =
            new IfThenElse(new And(new Superieur(new Variable("x"), new Constante(-20)), new Inferieur(new Variable("x"), new Constante(20))),
                             new Affectation (new Variable("y"), new Constante(12)),
                           new IfThenElse(
                               new And(new Superieur(new Variable("x"), new Constante(20)), new Inferieur(new Variable("x"), new Constante(40))),
                           new Affectation ( new Variable("y"), new Constante(32)),
                               new Affectation(new Variable("y"), new Constante(56))));
    Variable *x = new Variable("x", 5);
    Variable *y = new Variable("y");

    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;
    cout << *test << endl; test->eval();

    x->set(33);
    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;

    cout << *test << endl; test->eval();
    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;

    Variable::effacerMemoire();
    Expression::toutLiberer();

}

void testPour() {

     Variable *x = new Variable("x", 3.0);
     Variable *y = new Variable("y", 1);

     Pour * p = new Pour(new Affectation( new Variable("i"), new Constante(1.0)),
                         new InferieurEgal(new Variable("i"), new Variable("x")),
                         new Affectation(new Variable("i"), new Somme(new Variable("i") , new Constante(1.0))),
                         new Affectation(new Variable("y"), new Produit(new Variable("y"), new Variable("x"))));

    cout << "x = " << x->eval() << endl;
    cout << "y = " << y->eval() << endl;
    cout << *p << endl;

    cout << "x! = " << p->eval() << endl;

    x->set(5.0); y->set(1.0);
    cout << "x = " << x->eval() << endl;

    cout << "x! = " << p->eval() << endl;

    Variable::effacerMemoire();
    Expression::toutLiberer();
}

void testBloc() {

    Bloc * b = new Bloc("b1", new Affectation(new Variable("x"), new Constante(4)));

    b->add(new Affectation(new Variable("y"), new Produit(new Variable("x"), new Variable("x"))));
    b->add(new Somme(new Variable("x"), new Variable("y")));

    cout << *b << " = " << b->eval() << endl;

    Variable::effacerMemoire();
    Expression::toutLiberer();
}

void testDerivation() {

    Constante *c = new Constante(25);
    cout << "(" << *c << ") / dx " << " = " << *c->derive("x") << endl;

    Variable *v = new Variable("x");
    cout << "(" << *v << ") / dx " << " = " << *v->derive("x") << endl;
    cout << "(" << *v << ") / dy " << " = " << *v->derive("y") << endl;


    Sin *s = new Sin(new Exp(new Variable("x")));
    cout << "(" << *s << ") / dx " << " = " << *s->derive("x") << endl;

    Exp * e = new Exp(new Sin(new Constante(M_PI)));

    Division * d = new Division(s, e);
    Expression * ex = d->derive("x");

    cout << "(" << *d << ") / dx " << " = " << *ex << " = " << ex->eval() << endl;

    Variable::effacerMemoire();
    Expression::toutLiberer();
}

void testSimplification() {

    Expression * e = new Produit(
                new Constante(1),
                new Somme(
                    new Produit(new Constante(1), new Variable("x")),
                    new Produit(new Variable("x"), new Constante(1))
                          )
                            );

    cout << *e << endl;
    cout << *e->simplify() << endl;

}

void testPolynome() {

    Polynome * p1 = new Polynome(*(new Variable("x")));
    p1->set(-25, 0);
    p1->set(-1, 1);
    p1->set(-8, 2);

    Polynome * p2 = new Polynome(*(new Variable("x")));
    p2->set(36, 0);
    p2->set(18, 1);
    p2->set(22, 3);


    Polynome *p3 = new Polynome(*(new Variable("x")));
    p3->set(3,0);
    p3->set(2,1);
    p3->set(4,2);
    p3->set(5,3);

    cout << "(" <<*p1 << ")" << "+" << "(" <<  *p2 << ")" << " = "
         << *p1 + *p2 << endl;

    cout << "(" <<*p1 << ")" << "-" << "(" <<  *p2 << ")" << " = "
         << *p1 - *p2 << endl;

    cout << "(" <<*p1 << ")" << "*" << "(" <<  *p2 << ")" << " = "
         << *p1 * *p2 << endl;

    cout << "(" << *p1 << ")" << "+=" <<  *p2;
    *p1 += *p2;
    cout << "= " << *p1 << endl;

    cout << "(" << *p3 << ")' = "  << *p3->derive("x") << endl;

    Variable * x = new Variable("x");
    Affectation *xa = new Affectation(x, new Constante(5));
    cout << *xa << endl; xa->eval();
    cout << *p3 << " = "  << p3->eval() << endl;
}

extern int myparse(FILE*);
extern int myparseStdin();

int main(int argc, char *argv[]) {

    //cout << "dummy" << endl;

    //testUnaires();

    //testBinaires();

    //testVariables();

    //testConditionnel();

    //testSi_alors_sinon();

    //testSi_alors_sinon_2();

    //testPour();

    //testBloc();

    //testDerivation();

    //testSimplification();

//    vector<double> v(2);

//   v[0] = v[1] = 42;

//    v.resize(4);

//    v[2] = 10;


//    for(double d  : v)
//        cout << d << "  " ;


//    cout << "'" << v[100] << "'" << endl;
//    cout << v.size();

//    cout << endl;

//   TEST();

	if (argc != 1) {
		for (int i=1; i<argc; i++) {
			FILE *script = std::fopen(argv[i], "r");
			int res = myparse(script);
			if (res != 0) {
				return res;
			}
		}
		return 0;
	}
	else {	
		return myparseStdin();
	}
}



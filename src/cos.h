#ifndef COS_H
#define COS_H

#include "unaire.h"

/**
 * \brief Cos est la fonction Cosinus.
 * \ingroup binaires
 */
class Cos : public Unaire
{
public:
    Cos(Expression*);

    virtual double eval();

    virtual Expression* clone();
    
    virtual const string toString() const;

protected:
        virtual Expression* _derive(const string&);
};

#endif // COS_H

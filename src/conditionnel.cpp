#include "conditionnel.h"

Conditionnel::Conditionnel(Expression * e1, Expression * e2, Expression * e3) :
    Ternaire::Ternaire(e1,e2,e3)
{
    if(!(e1 && e2 && e3))
        throw std::invalid_argument("Conditionnel: should have all operands != NULL");
}

double Conditionnel::eval()
{
    return (e1->eval())?e2->eval():e3->eval();
}

Expression *Conditionnel::clone()
{
    return new Conditionnel(e1,e2,e3);
}

Expression *Conditionnel::simplify()
{
    return this;
}

ostream &Conditionnel::what(ostream & os) const
{
    return os << *e1 << " ? "  << *e2 << " : " << *e3;
}

const string Conditionnel::toString() const {
    return "ternaire";
}

#ifndef SOMME_H
#define SOMME_H

#include "binaire.h"

/**
 * \brief Opérateur addition.
 * \ingroup binaires
 */
class Somme : public Binaire
{
public:
    Somme(Expression *,Expression *);

    virtual double eval();

    virtual Expression* derive(const string&);

    virtual Expression* simplify();

    virtual Expression* clone();
    
    virtual const string toString() const;
};

#endif // SOMME_H

#include "nonegal.h"


NonEgal::NonEgal(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double NonEgal::eval()
{
    return (e1->eval() != e2->eval())?1:0;
}

Expression *NonEgal::clone()
{
    return new NonEgal(e1->clone(), e2->clone());
}

Expression *NonEgal::simplify()
{
    return new NonEgal(e1->simplify(), e2->simplify());
}

const string NonEgal::toString() const {
    return "!=";
}

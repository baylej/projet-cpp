#ifndef MOINSUNAIRE_H
#define MOINSUNAIRE_H

#include "unaire.h"

/**
 * \brief Moins unaire.
 * \ingroup unaires
 */
class MoinsUnaire : public Unaire
{
public:
    MoinsUnaire(Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // MOINSUNAIRE_H

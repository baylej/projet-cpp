#include "inferieuregal.h"


InferieurEgal::InferieurEgal(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double InferieurEgal::eval()
{
    return (e1->eval() <= e2->eval())?1:0;
}

Expression *InferieurEgal::clone()
{
    return new InferieurEgal(e1->clone(), e2->clone());
}

Expression *InferieurEgal::simplify()
{
    return this;
}

const string InferieurEgal::toString() const {
    return "<=";
}

#include "superieuregal.h"


SuperieurEgal::SuperieurEgal(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double SuperieurEgal::eval()
{
    return (e1->eval() >= e2->eval())?1:0;
}

Expression *SuperieurEgal::clone()
{
    return new SuperieurEgal(e1->clone(), e2->clone());
}

Expression *SuperieurEgal::simplify()
{
    return new SuperieurEgal(e1->simplify(), e2->simplify());
}

const string SuperieurEgal::toString() const {
    return ">=";
}

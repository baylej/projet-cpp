#ifndef INFERIEUR_H
#define INFERIEUR_H

#include "binaire.h"

/**
 * \brief Comparateur d'infériorité stricte "<".
 * \ingroup binaires
 * \ingroup comparateurs
 */
class Inferieur : public Binaire
{
public:
    Inferieur(Expression *,Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // INFERIEUR_H

#include "division.h"
#include "difference.h"
#include "produit.h"
#include "constante.h"

Division::Division(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double Division::eval()
{
    return e1->eval() / e2->eval();
}

Expression *Division::derive(const string & v)
{
    return new Division(
                new Difference(
                    new Produit(e1->derive(v), e2),
                    new Produit(e2->derive(v), e1)),
                new Produit(e2,e2)
                );
}

Expression *Division::simplify()
{
    Constante *c;

    if((c = dynamic_cast<Constante*>(e2))) {
       if(c->eval() == 1.0) {
           return e1->simplify();
       }
    }
    Expression *s1 = e1->simplify();
    Expression *s2 = e2->simplify();

    if(s1 != e1 && s2 != e2) {
        return (new Division(e1->simplify(), e2->simplify()))->simplify();
    }
    else if(s1 != e1) {
        return (new Division(e1->simplify(), e2))->simplify();
    }
    else if(s2 != e2) {
        return (new Division(e1, e2->simplify()))->simplify();
    }
    return this;
}

Expression *Division::clone()
{
    return new Division(e1->clone(), e2->clone());
}

const string Division::toString() const {
    return "/";
}

#include <cmath>

#include "sin.h"
#include "cos.h"

Sin::Sin(Expression * e1) : Unaire::Unaire(e1)
{
}

double Sin::eval()
{
    return sin(e1->eval());
}

Expression *Sin::clone()
{
    return new Sin(e1->clone());
}

Expression *Sin::_derive(const string &)
{
    return new Cos(e1);
}

const string Sin::toString() const {
    return "sin";
}

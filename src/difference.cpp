#include "difference.h"
#include "variable.h"
#include "constante.h"

Difference::Difference(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double Difference::eval()
{
    return e1->eval() - e2->eval();
}

Expression *Difference::derive(const string & v)
{
    return new Difference(e1->derive(v), e2->derive(v));
}

Expression *Difference::simplify()
{
    Variable * v1 = dynamic_cast<Variable*> (e1);
    Variable * v2 = dynamic_cast<Variable*> (e2);

    Constante * c1 = dynamic_cast<Constante*> (e1);
    Constante * c2 = dynamic_cast<Constante*> (e2);

    if (v1 && v2) {
        if(*v1 == *v2) {
            return new Constante(0);
        }
    } else if( c1 && c2) {
        return new Constante(c1->eval() - c2->eval());
    }
    Expression *s1 = e1->simplify();
    Expression *s2 = e2->simplify();

    if(s1 != e1 && s2 != e2) {
        return (new Difference(e1->simplify(), e2->simplify()))->simplify();
    }
    else if(s1 != e1) {
        return (new Difference(e1->simplify(), e2))->simplify();
    }
    else if(s2 != e2) {
        return (new Difference(e1, e2->simplify()))->simplify();
    }
    return this;
}

Expression *Difference::clone()
{
    return new Difference(e1->clone(),e2->clone());
}

const string Difference::toString() const {
    return "-";
}

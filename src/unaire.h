#ifndef UNAIRE_H
#define UNAIRE_H

#include "expression.h"

/**
 * \brief Représente les opérateurs unaires.
 * Cette classe comporte des méthodes virtuelles.
 */
class Unaire : public Expression
{
public:

    Expression* derive(const string&);
    
    virtual void dotWhat(stringstream &vertices, stringstream &edges) const;

protected:
    Unaire(Expression * = 0);
    Expression * e1;

    virtual Expression* _derive(const string&);

    virtual Expression* simplify();

    virtual ostream& what(ostream &) const;

private:
    Unaire(const Unaire&);
    Unaire &operator=(const Unaire&);
};

#endif // UNAIRE_H

#ifndef SUPERIEUR_H
#define SUPERIEUR_H

#include "binaire.h"

/**
 * \brief Comparateur de supériorité stricte ">".
 * \ingroup binaires
 * \ingroup comparateurs
 */
class Superieur : public Binaire
{
public:
    Superieur(Expression *,Expression *);

    virtual double eval();

    virtual Expression* simplify();

    virtual Expression* clone();
    
    virtual const string toString() const;
};

#endif // SUPERIEUR_H

#ifndef SUPERIEUREGAL_H
#define SUPERIEUREGAL_H

#include "binaire.h"

/**
 * \brief Comparateur de supériorité ">=".
 * \ingroup binaires
 * \ingroup comparateurs
 */
class SuperieurEgal : public Binaire
{
public:
    SuperieurEgal(Expression *,Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // SUPERIEUREGAL_H

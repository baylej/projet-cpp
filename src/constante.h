#ifndef CONST_H
#define CONST_H

#include "expression.h"

/**
 * \brief Littéral à valeur constante.
 * \ingroup litteraux
 */
class Constante : public Expression
{
public:
	/// Crée une constante avec sa valeur.
    Constante(double);

    virtual double eval();

    virtual Expression* derive(const string&);

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;

protected:
    virtual ostream& what(ostream &) const;
    virtual string what() const { return "";}

private:
    double value;
};

#endif // CONST_H

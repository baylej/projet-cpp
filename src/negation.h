#ifndef NEGATION_H
#define NEGATION_H

#include "unaire.h"

/**
 * \brief Non logique.
 * \ingroup unaires
 */
class Negation : public Unaire
{
public:
    Negation(Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // NEGATION_H

#ifndef EXP_H
#define EXP_H

#include "unaire.h"

/**
 * \brief Exp est l'opérateur exponentiel.
 * \ingroup binaires
 */
class Exp : public Unaire
{
public:
    Exp(Expression *);

    virtual double eval();

    virtual Expression* clone();
    
    virtual const string toString() const;

protected:
        virtual Expression* _derive(const string&);
};

#endif // EXP_H

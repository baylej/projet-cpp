#include <cmath>

#include "cos.h"
#include "sin.h"

Cos::Cos(Expression * e1) : Unaire::Unaire(e1)
{
}

double Cos::eval()
{
    return cos(e1->eval());
}

Expression *Cos::clone()
{
    return new Cos(e1->clone());
}

Expression *Cos::_derive(const string &)
{
    return new Sin(e1);
}

const string Cos::toString() const {
	return "cos";
}

#include "egalegal.h"

EgalEgal::EgalEgal(Expression * e1, Expression * e2) : Binaire::Binaire(e1,e2)
{
}

double EgalEgal::eval()
{
    return (e1->eval() == e2->eval())?1:0;
}

Expression *EgalEgal::clone()
{
    return new EgalEgal(e1->clone(), e2->clone());
}

Expression *EgalEgal::simplify()
{
    return new EgalEgal(e1->simplify(), e2->simplify());
}

const string EgalEgal::toString() const {
    return "==";
}

#include "ternaire.h"

Ternaire::Ternaire(Expression *_e1, Expression *_e2, Expression *_e3) :
    Expression::Expression(), e1(_e1), e2(_e2), e3(_e3)
{
}

const string Ternaire::toString() const {
    return "ternaire";
}

void Ternaire::dotWhat(stringstream &vertices, stringstream &edges) const {
	vertices << (size_t)this;
	vertices << " [label=\"" << toString() << "\" shape=diamond];" << endl;
	
	edges << (size_t)this;
	edges << " -> " << (size_t)e1;
	edges << " [style=dashed];" << endl;
	
	edges << (size_t)this;
	edges << " -> " << (size_t)e2;
	edges << ";" << endl;
	
	if (e3) {
		edges << (size_t)this;
		edges << " -> " << (size_t)e3;
		edges << ";" << endl;
		
		e1->dotWhat(vertices, edges);
		e2->dotWhat(vertices, edges);
		e3->dotWhat(vertices, edges);
	}
	else {
		e1->dotWhat(vertices, edges);
		e2->dotWhat(vertices, edges);
	}
}


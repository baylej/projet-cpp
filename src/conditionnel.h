#ifndef CONDITIONNEL_H
#define CONDITIONNEL_H

#include "ternaire.h"

/**
 * \brief Contrôle de flux.
 * Conditionnel est l'opérateur ternaire du C, permet de choisir entre deux instructions selon une condition.
 * \ingroup ternaires
 */
class Conditionnel : public Ternaire
{
public:
    Conditionnel(Expression *,Expression *,Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;

protected:
    virtual ostream& what(ostream &) const;

};

#endif // CONDITIONNEL_H

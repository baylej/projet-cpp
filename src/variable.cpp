#include <iostream>

#include "variable.h"
#include "constante.h"

map<string, double> Variable::_pool;

Variable::Variable(string s) : Expression::Expression(), key(s)
{
}

Variable::Variable(string s , double d) : Expression::Expression(), key(s)
{
    _pool[s] = d;
}

void Variable::set(double d)
{
    _pool[key] = d;
}

void Variable::effacerMemoire()
{
    _pool.clear();
}

double Variable::eval()
{
    return _pool[key];
}

Expression *Variable::derive(const string & key)
{
    return new Constante((key == this->key) ? 1 : 0);
}

Expression *Variable::simplify()
{
    return this;
}

Expression *Variable::clone()
{
    return new Variable(key);
}

bool Variable::operator==(const Variable & v) const
{
    return this->key == v.key;
}

ostream &Variable::what(ostream & os) const
{
    return os << toString();
}

const string Variable::toString() const {
    return key;
}

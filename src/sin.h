#ifndef SIN_H
#define SIN_H

#include "unaire.h"

/**
 * \brief Sin est la fonction sinus.
 * \ingroup binaires
 */
class Sin : public Unaire
{
public:
    Sin(Expression *);

    virtual double eval();

    virtual Expression* clone();
    
    virtual const string toString() const;

protected:
    Expression* _derive(const string&);
};

#endif // SIN_H

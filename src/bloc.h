#ifndef BLOC_H
#define BLOC_H

#include <list>

#include "expression.h"

/**
 * \brief Regroupe des instructions dans un bloc.
 */
class Bloc : public Expression
{
public:

    Bloc(const char *, Expression*);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();

    void add(Expression *);
    
    virtual const string toString() const;
    virtual void dotWhat(stringstream &vertices, stringstream &edges) const;

protected:
    virtual ostream& what(ostream &) const;

private:
    list<Expression*> _pool;
    const char * _name;

    Bloc(const Bloc&);
    Bloc &operator=(const Bloc&);
};

#endif // BLOC_H

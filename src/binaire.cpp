#include "binaire.h"

Binaire::Binaire(Expression *_e1, Expression *_e2) : Expression::Expression(), e1(_e1), e2(_e2)
{
    if(!(e1 && e2))
        throw std::invalid_argument("Binaire : Binary operator should always have two valid operands");
}

ostream &Binaire::what(ostream & os) const
{
    return os << "(" << *e1 << " " << toString() << " " << *e2 << ")";
}

void Binaire::dotWhat(stringstream &vertices, stringstream &edges) const {
	vertices << (size_t)this;
	vertices << " [label=\"" << toString() << "\"];" << endl;
	
	edges << (size_t)this;
	edges << " -> " << (size_t)e1;
	edges << ";" << endl;
	
	edges << (size_t)this;
	edges << " -> " << (size_t)e2;
	edges << ";" << endl;
	
	e1->dotWhat(vertices, edges);
	e2->dotWhat(vertices, edges);
}


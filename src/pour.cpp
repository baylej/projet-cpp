#include "pour.h"
#include "constante.h"

Pour::Pour(Expression * _init, Expression * _cond, Expression * _loop, Expression * _body) :
    init(_init), cond(_cond), loop(_loop), body(_body)
{
    if(!(_cond && _body))
        throw std::invalid_argument("Pour: should have at least a loop condition and a body");
}

double Pour::eval()
{
    double tmp(0);
    Constante c(0);

    for(((init)?init:&c)->eval(); cond->eval(); ((loop)?loop:&c)->eval()) {
        tmp = body->eval();
    }
    return tmp;
}

Expression *Pour::simplify()
{
    return this;
}

Expression *Pour::clone()
{
    return new Pour(init,cond,loop,body);
}

ostream &Pour::what(ostream & os) const
{
    Constante c(0);
    return os << "for (" << ((init)?*init:c) << "; " << *cond << "; " << ((loop)?*loop:c) << ") {" << endl
                 << "\t" << *body << endl
              << "}" ;
}

const string Pour::toString() const {
    return "for";
}

void Pour::dotWhat(stringstream &vertices, stringstream &edges) const {
	vertices << (size_t)this;
	vertices << " [label=\"" << toString() << "\" shape=pentagon];" << endl;
	
	edges << (size_t)this;
	edges << " -> " << (size_t)cond;
	edges << " [style=dashed];" << endl;
	
	edges << (size_t)this;
	edges << " -> " << (size_t)body;
	edges << ";" << endl;
	
	if (init) {
		edges << (size_t)this;
		edges << " -> " << (size_t)init;
		edges << " [style=dashed];" << endl;
	}
	
	if (loop) {
		edges << (size_t)this;
		edges << " -> " << (size_t)loop;
		edges << " [style=dashed];" << endl;
	}
	
	if (init) init->dotWhat(vertices, edges);
	cond->dotWhat(vertices, edges);
	if (loop) loop->dotWhat(vertices, edges);
	body->dotWhat(vertices, edges);
}

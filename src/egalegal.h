#ifndef EGALEGAL_H
#define EGALEGAL_H

#include "binaire.h"

/**
 * \brief Comparateur d'égalité "==".
 * \ingroup binaires
 * \ingroup comparateurs
 */
class EgalEgal : public Binaire
{
public:
    EgalEgal(Expression *,Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // EGALEGAL_H

#ifndef PRODUIT_H
#define PRODUIT_H

#include "binaire.h"

/**
 * \brief Opérateur multiplication.
 * \ingroup binaires
 */
class Produit : public Binaire
{
public:
    Produit(Expression *,Expression *);

    virtual double eval();

    virtual Expression* derive(const string&);

    virtual Expression* simplify();

    virtual Expression* clone();
    
    virtual const string toString() const;
};

#endif // PRODUIT_H

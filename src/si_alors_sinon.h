#ifndef IF_THEN_ELSE_H
#define IF_THEN_ELSE_H

#include "ternaire.h"

/**
 * \brief Contrôle de flux.
 * Si ( condition ) alors { instruction } Sinon { instruction }.
 */
class IfThenElse : public Ternaire
{
public:
	/// Crée un IfThenElse avec une condition, une expression à exécuter si vrai et une expression à exécuter si faux.
	/// Le troisième paramètre est optionnel et peut-être NULL.
    IfThenElse(Expression *, Expression *, Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;

protected:
    virtual ostream& what(ostream &) const;
};

#endif // IF_THEN_ELSE_H

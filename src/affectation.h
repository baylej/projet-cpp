#ifndef AFFECTATION_H
#define AFFECTATION_H

#include "binaire.h"
#include "variable.h"

/**
 * \brief Affectation d'une valeur dans une variable "=".
 * \ingroup binaires
 */
class Affectation : public Binaire
{
public:
    Affectation(Variable *, Expression *);

    virtual double eval();

    virtual Expression* clone();

    virtual Expression* simplify();
    
    virtual const string toString() const;
};

#endif // AFFECTATION_H

CC= g++
CFLAGS= -Wall -Wextra -Weffc++ -std=gnu++11 -g -c
LFLAGS= -lm
SRCDIR= src
SOURCES= $(wildcard $(SRCDIR)/*.cpp)
OBJECTS= $(SOURCES:.cpp=.o) src/anasynt.o src/analex.o
TARGET= arithmetics

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) $(LFLAGS) $^ -o $@

%.o: %.cpp
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) $^ -o $@

src/anasynt.c: pre/anasynt.y
	bison -v -d -o $@ $^

src/analex.c: pre/analex.l src/anasynt.c
	flex -o $@ $<

.PHONY: clean mrproper

.IGNORE: clean mrproper

clean:
	rm $(SRCDIR)/*.o src/anasynt.[ch] src/analex.c

mrproper: clean
	rm $(TARGET)


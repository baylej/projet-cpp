%option noyywrap
%option nounput

%{
#include <string.h>

#include "expression.h"
#include "anasynt.h"
%}

%%
 /* NETTOYAGE */
"/*"[^*/]+"*/" ;       /* Supprime les commentaires blocks */
"//".+"\n" ;           /* Supprime les commentaires fin de ligne */
[ \t\n]+ ;             /* Supprime les espaces */

 /* OPERATORS */
"="  {return EGAL;}
"+"  {return ADD;}
"-"  {return SUB;}
"*"  {return STAR;}
"/"  {return DIV;}
"!"  {return NEG;}
"'"  {return DERIV;}
"==" {return EQ;}
"!=" {return NEQ;}
"<"  {return INF;}
">"  {return SUP;}
"<=" {return INFEQ;}
">=" {return SUPEQ;}
"&&" {return AND;}
"||" {return OR;}
";"  {return PV;}
","  {return VRG;}
"("  {return LPAR;}
")"  {return RPAR;}
"["  {return LBRA;}
"]"  {return RBRA;}
"{"  {return LACC;}
"}"  {return RACC;}
"?"  {return TERN;}
":"  {return TERS;}
"^"  {return POW;}
"$"  {return DISP;}
"%"  {return ANS;}

 /* CONTROLE DE FLUX */
"if"    {return IF;}
"else"  {return ELSE;}
"for"   {return FOR;}

 /* RAPPELLE UNE EXPRESSION DEJA EVALUEE */
"#"[0-9]+ {sscanf(yytext+1,"%lf",&yylval.num); return PREV;}

 /* IDENTIFIEUR */
[a-zA-Z][_a-zA-Z0-9]* {memcpy(yylval.ident, yytext, yyleng); yylval.ident[yyleng] = '\0'; return IDENT;}

 /* VALEURS */
[0-9]+"."?[0-9]*      {sscanf(yytext,"%lf",&yylval.num); return NUM;}

. return yytext[0];
%%


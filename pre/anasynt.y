%{
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <stack>
#include <iostream>
#include <fstream>
#include <vector>

#include "expression.h"

#include "sin.h"
#include "cos.h"
#include "constante.h"
#include "exp.h"

#include "somme.h"
#include "difference.h"
#include "produit.h"
#include "division.h"
#include "inferieuregal.h"
#include "superieuregal.h"
#include "superieur.h"
#include "inferieur.h"
#include "egalegal.h"
#include "nonegal.h"

#include "moinsunaire.h"

#include "and.h"
#include "or.h"
#include "negation.h"

#include "variable.h"
#include "affectation.h"

#include "ternaire.h"
#include "conditionnel.h"

#include "si_alors_sinon.h"
#include "pour.h"
#include "bloc.h"

#include "polynome.h"

using std::printf;
using std::strncmp;
using std::cout;
using std::endl;
using std::ofstream;
using std::system;

int yyerror(char*);
int yylex();
extern FILE *yyin;

/* Contains the blocs we are currently in */
bool is_new_bloc = false;
stack<Bloc*> blocs;

/* Contains the polynome we are parsing */
Polynome *polynome = NULL;

/* Contains the very last instruction */
Expression *ans = NULL;

/* Counts how many instruction have been eval'd */
unsigned int instr_count;

/* Contains all the eval'd instruction */
vector<Expression*> expressions;

#ifdef DEBUG
#  define yydbg(...) printf(__VA_ARGS__)
#else
#  define yydbg(...)
#endif

Expression* parse_function(char ident[65], Expression *param) {
	Expression *res = NULL;
	if (!strncmp(ident, "sin", 65))
		res = new Sin(param);
	
	else if (!strncmp(ident, "cos", 65))
		res = new Cos(param);
	/*
	else if (!strncmp(ident, "tan", 65))
		res = new Tan(param);*/
	
	else if (!strncmp(ident, "exp", 65))
		res = new Exp(param);
	
	else if (!strncmp(ident, "clean", 65)) {
		Expression::toutLiberer();
		Variable::effacerMemoire();
		instr_count = 0;
		expressions.clear();
		ans = NULL;
	}
	
	else if (!strncmp(ident, "simp", 65))
		res = param->simplify();
	
	else if (!strncmp(ident, "mkdot", 65))
		cout << param->makeDot() << endl;
	
	else if (!strncmp(ident, "dispdot", 65)) {
		ofstream file("/tmp/dot.txt"); // FIXME not portable, posix only.
		file << param->makeDot();
		file.close();
		system("dot -Txlib /tmp/dot.txt");
	}
	
	else
		cout << "unknown function " << ident << endl;
	
	return res;
}

%}

/* YYLVAL */
%union {
	char ident[65];
	double num;
	Expression *expptr;
	struct _monome {
		int coeff;
		unsigned int degre;
	} monome;
}

/* TOKENS */
%token EGAL ADD SUB DIV STAR NEG EQ NEQ DERIV INF SUP INFEQ SUPEQ AND OR POW DISP ANS
%token PV VRG LPAR RPAR LBRA RBRA LACC RACC
%token IF ELSE FOR TERN TERS
%token <ident> IDENT
%token <num> NUM PREV

%type <expptr> EExp Exp Arguments Instr
%type <monome> Monome

%nonassoc LOWPRIO

%right EGAL TERN TERS NEG
%left AND OR
%left EQ NEQ
%left INF SUP INFEQ SUPEQ
%left ADD SUB
%left DIV STAR
%left DERIV POW

%nonassoc NELSE
%nonassoc ELSE

%nonassoc HIGHPRIO

%%
SuiteInstr: SuiteInstr Instr {expressions.push_back($2); ans=$2; if ($2 != NULL) printf("#%d: %lf\n", ++instr_count, $2->eval()); else {yydbg("ne peut evaluer instruction nulle.");}}
          | SuiteInstr DISP Instr {expressions.push_back($3); ans=$3; if ($3 != NULL) {cout << *($3) << endl; printf("#%d: %lf\n", ++instr_count, $3->eval());} else {yydbg("ne peut evaluer instruction nulle.");}}
          |/*E*/
;

InstrBloc: InstrBloc Instr {yydbg("add Exp => Bloc"); if (is_new_bloc) {blocs.push(new Bloc(NULL, $2)); is_new_bloc=false;} else blocs.top()->add($2);}
         | /*E*/
;

CREATEBLOC: {yydbg("new block"); is_new_bloc=true; }
;

Instr: IF LPAR Exp RPAR Instr %prec NELSE {yydbg("if sans else\n"); $$ = new IfThenElse($3, $5, NULL);}
     | IF LPAR Exp RPAR Instr ELSE Instr {yydbg("if else\n"); $$ = new IfThenElse($3, $5, $7);}
     | FOR LPAR EExp PV Exp PV EExp RPAR Instr {yydbg("for \n"); $$ = new Pour($3, $5, $7, $9);}
     | LACC CREATEBLOC InstrBloc RACC {yydbg("bloc d'instruction"); if (is_new_bloc) {is_new_bloc=false; $$=NULL;} else {$$ = blocs.top(); blocs.pop();}}
     | EExp PV {yydbg("instruction\n"); $$ = $1;}
;

Arguments: Arguments VRG Exp {$$ = $1;} /* FIXME */
         | Exp     {$$ = $1;}
         | /*E*/   {$$ = NULL;}
;

EExp: Exp %prec LOWPRIO {$$ = $1;}
    | /*E*/             {$$ = NULL;}
;

Exp: ANS {yydbg("expression precedente"); $$ = ans;} /* FIXME use ->clone() ?? */
   | PREV {yydbg("instruction #%d\n", $1); $$ = expressions.at($1-1);} /* FIXME use ->clone() ?? */
   | IDENT {yydbg("valeur de `%s`\n", $1); $$ = new Variable($1);}
   | NUM   {yydbg("nombre %f\n",$1); $$ = new Constante($1);}
   | Exp AND Exp {yydbg("and\n"); $$ = new And($1, $3);}
   | Exp  OR Exp {yydbg("or\n"); $$ = new Or($1, $3);}
   | NEG Exp     {yydbg("neg\n"); $$ = new Negation($2);}
   
   | IDENT EGAL Exp {yydbg("Affectation var : %s\n",$1); $$ = new Affectation(new Variable($1), $3);}
   
   | Exp    EQ Exp {yydbg("eq\n"); $$ = new EgalEgal($1, $3);}
   | Exp   NEQ Exp {yydbg("neq\n"); $$ = new NonEgal($1, $3);}
   | Exp   INF Exp {yydbg("inf\n"); $$ = new Inferieur($1, $3);}
   | Exp   SUP Exp {yydbg("sup\n"); $$ = new Superieur($1, $3);}
   | Exp INFEQ Exp {yydbg("infeq\n"); $$ = new InferieurEgal($1, $3);}
   | Exp SUPEQ Exp {yydbg("supeq\n"); $$ = new SuperieurEgal($1, $3);}
   | Exp TERN Exp TERS Exp {yydbg("ternaire\n"); $$ = new Conditionnel($1, $3, $5);}
   
   | Exp ADD Exp {yydbg("add\n"); $$ = new Somme($1, $3);}
   | Exp SUB Exp {yydbg("sub\n"); $$ = new Difference($1, $3);}
   
   | Exp STAR Exp {yydbg("star\n"); $$ = new Produit($1, $3);}
   | Exp  DIV Exp {yydbg("div\n"); $$ = new Division($1, $3);}
   
   | SUB Exp %prec HIGHPRIO {yydbg("sub unaire\n"); $$ = new MoinsUnaire($2);}
   | Exp DERIV IDENT {yydbg("deriv selon %s\n", $3); $$ = $1->derive($3);}
   | LPAR Exp RPAR {yydbg("entre parentheses\n"); $$ = $2;}
   | IDENT LPAR Arguments RPAR {yydbg("fonction '%s'\n", $1); $$ = parse_function($1, $3);}
   | CreatePolynome LACC Polynome RACC {yydbg("Polynome\n"); $$ = polynome;}
;

CreatePolynome: IDENT LBRA IDENT RBRA {Variable v($3); polynome = new Polynome(v);}
;

Polynome: Polynome ADD Monome {polynome->set( $3.coeff, $3.degre);}
        | Polynome SUB Monome {polynome->set(-$3.coeff, $3.degre);}
        | Monome     {polynome->set( $1.coeff, $1.degre);}
        | SUB Monome {polynome->set(-$2.coeff, $2.degre);}
;

Monome: NUM STAR IDENT POW NUM {$$.coeff = (int)$1; $$.degre = (unsigned int)$5;}
      | IDENT POW NUM {$$.coeff = 1; $$.degre = (unsigned int)$3;}
      | NUM STAR IDENT {$$.coeff = (int)$1; $$.degre = 1;}
      | IDENT {$$.coeff = 1; $$.degre = 1;}
      | NUM {$$.coeff = (int)$1; $$.degre = 0;}
;
%%

int yyerror(char *s) {
	fprintf(stderr,"%s\n",s);
	return 0;
}

int myparse(FILE * in) {
	yyin = in;
	return yyparse();
}

int myparseStdin() {
	return myparse(stdin);
}

